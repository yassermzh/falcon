List all videos
===========
- GET `/videos`
- parameters:
    -   node: Filter to sepecified node. comma separated. Example: `node1,node2`
    -   page
    -   per_page
    -   sort
    -   order

Show video
===========
- GET `/videos/{id}`

Upload video
===========
- POST `/videos`
- parameters
   - name

Edit video
==========
- PUT `/videos/{id}`
- parameteres:
    -  node: Filter to sepecified node. comma separated. Example: `node1,node2`

Add video replications
===========
- POST `/videos/{id}/replications`
- parameteres:
    -  node: All nodes to be replicated to. Default value is `all`. comma separated. Example: `node1,node2`

Delete video replications
===========
- DELETE `/videos/{id}/replications`
- parameteres:
    -  node: Delete replications. comma separated. Default value is ``. Example: `node1,node2`
    
Change status
==========
 - POST `/nodes/{id}/statuses`
 - parameters
    - state: `active|inactive`
 - ref: https://developer.github.com/v3/repos/deployments/#create-a-deployment-status
 
