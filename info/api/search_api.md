Search videos
===========
- GET `/search/videos`
- parameters:
    -   q: string with qualifier 
    -   sort: The sort field. One of stars, forks, or updated. Default: results are sorted by best match.
    -   order: The sort order if sort parameter is provided. One of asc or desc. Default: desc
