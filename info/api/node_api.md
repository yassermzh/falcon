List all nodes
===========
- GET `/nodes`
- parameters: 
    - q: search in nodes (no elasticsearch here!) 
    - state: `active|inactive`

Show node
===========
- GET `/nodes/{id}`

Register node
===========
- POST `/nodes`

Delete node
===========
- DELETE `/nodes/{id}`

Edit node
==========
- PUT `/nodes/{id}`

Change status
==========
 - POST `/nodes/{id}/statuses`
 - parameters
    - state: `active|inactive`
    - default: `true|false`
 - ref: https://developer.github.com/v3/repos/deployments/#create-a-deployment-status
 

