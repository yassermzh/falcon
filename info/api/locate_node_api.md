Locate node given user info
===========
- GET `/locate_node`
- parameters:
    -   ip_address: optional
    -   resolution: `280|360|720`
    -   video_id