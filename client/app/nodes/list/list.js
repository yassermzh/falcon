angular.module('nodes.list', [
    'resources.node', 'av.toggleButton',
    'av.filters', 'av.sortGrid', 'av.contentHeader'])

    .config(function ($routeProvider) {

        'use strict';
        $routeProvider
            .when('/nodes', {
                templateUrl: 'app/nodes/list/list.html',
                controller: 'NodesListCtrl',
                resolve: {
                    nodes: function(Node) {
                        return Node.query();
                    }
                }
            });
    })

    .controller('NodesListCtrl', function($scope, nodes){

        'use strict';
        $scope.nodes = nodes;
        console.log('nodesListCtrl called!');

        $scope.order = {
            field: 'bandwidth',
            reverse: false
        };

        $scope.toggleActivation = function(item){
            console.log('toggleActivation item=', item);
            item.setActivation(!item.isActive)
                .then(function(newItem){
                    _.merge(item, newItem);
                });
        };

    });
