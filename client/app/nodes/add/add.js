'use strict';

angular.module('nodes.add', ['resources.node', 'av.contentHeader'])
    .config(function ($routeProvider) {


        $routeProvider
            .when('/nodes/add', {
                templateUrl: 'app/nodes/add/add.html',
                controller: 'NodesAddCtrl',
                resolve: {
                    node: function (Node) {
                        return Node.build();
                    }
                }
            })
            .when('/nodes/:id', {
                templateUrl: 'app/nodes/add/add.html',
                controller: 'NodesAddCtrl',
                resolve: {
                    node: function($route, Node) {
                        var id = $route.current.params.id;
                        return Node.get(id);
                    }
                }
            });
    })

    .controller('NodesAddCtrl', function($scope, $timeout, Node, node){

        'use strict';

        console.log('nodesAddCtrl called!');

        $scope.node = node;
        var isNew = !node.id;

        $scope.submit = function() {
            if (isNew) {
                $scope.node.add()
                    .then(function(newNode){
                        $scope.node = newNode;
                        showMessage();
                    });
            } else {
                $scope.node.edit()
                    .then(function(newNode){
                        $scope.node = newNode;
                        showMessage();
                    });
            }
        };

        $scope.setDefault = function() {
            Node.setStatuses({default: node.id})
                .then(function(newNode){
                    $scope.node = newNode;
                    showMessage();
                });

        };

        function showMessage() {
            $scope.showMessage = true;
            $timeout(function(){
                $scope.showMessage = false
            }, 1000);
        };

    });
