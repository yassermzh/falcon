'use strict';

angular.module('files.add', [
    'resources.file', 'ngFileUpload',
    "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls"])

    .config(function ($routeProvider) {

        'use strict';

        $routeProvider
            .when('/files/add', {
                templateUrl: 'app/files/add/add.html',
                controller: 'FilesAddCtrl',
                resolve: {
                    file: function (File) {
                        return File.build();
                    }
                }
            })
            .when('/files/:id', {
                templateUrl: 'app/files/add/add.html',
                controller: 'FilesAddCtrl',
                resolve: {
                    file: function($route, File) {
                        var id = $route.current.params.id;
                        return File.get(id);
                    }
                }
            });
    })

    .controller('FilesAddCtrl', function($scope, $timeout, Upload, $sce, file, fileConfig){

        console.log('filesAddCtrl called!, file=', file);

        $scope.file = file;
        $scope.isNew = !file.id;

        if (!$scope.isNew) {
            $scope.sources = [{
                src: $sce.trustAsResourceUrl(file.path),
                type: "video/mp4"
            }];
        }

        function showMessage() {
            $scope.showMessage = true;
            $timeout(function(){
                $scope.showMessage = false;
            }, 1000);
        };

        $scope.save = function() {
            console.log('file> save called');
            $scope.file.edit().then(function(file){
                $scope.file = file;
                $scope.showMessage = true;
                showMessage();
            });
        };

        // upload later on form submit or something similar
        // if (form.file.$valid && $scope.file && !$scope.file.$error) {
        //     $scope.upload($scope.file);
        // }

        // upload on file select or drop
        $scope.upload = function (file) {
            Upload.upload({
                url: fileConfig.url,
                data: {file: file, 'username': $scope.username}
            }).then(function (res) {
                _.merge($scope.file, res.data);
                $scope.isNew = false;
                console.log('Success ' + res.config.data.file.name +
                            'uploaded. Resonse: ' + res.data);
            }, function (res) {
                console.log('Error status: ' + res.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

    });
