angular.module('files.list', ['resources.file', 'av.toggleButton'])
    .config(function ($routeProvider) {

        'use strict';

        $routeProvider
            .when('/files', {
                templateUrl: 'app/files/list/list.html',
                controller: 'FilesListCtrl',
                resolve: {
                    files: function(File) {
                        return File.query();
                    }
                }
            });
    })
    .controller('FilesListCtrl', function($scope, files){

        'use strict';
        $scope.files = files;
        console.log('filesListCtrl called!');

        $scope.toggleActivation = function(item){
            console.log('toggleActivation item=', item);
            item.setActivation(!item.isActive)
                .then(function(newItem){
                    _.merge(item, newItem);
                });
        };

    });
