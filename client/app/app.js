'use strict';

angular.module('falconApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'nodes',
    'files',
    'av.sidebar',
    'av.navbar'
])
    .run(function($rootScope){
        $rootScope.sitePrefix = '#';
    })
    .config(function ($routeProvider, $locationProvider) {

        $routeProvider
            .otherwise({
                redirectTo: '/'
            });

        //$locationProvider.html5Mode(true);
    });
