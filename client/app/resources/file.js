angular.module('resources.file', [])
    .constant("fileConfig", {
        url: "/api/v1/files",
        statusesUrl: "/api/v1/files/%s/statuses"
    })
    .factory('File', function($http, fileConfig) {

        function File(data) {
            _.merge(this, data);
        }

        File.query = function() {
            return $http.get(fileConfig.url)
                .then(function(res){
                    return _.map(res.data, function(d){ return File.build(d); });
                });
        };

        File.prototype.add = function() {
            return $http.post(fileConfig.url)
                .then(function(res){
                    return new File(res.data);
                });
        };

        File.prototype.edit = function() {
            return $http.put(fileConfig.url + '/' + this.id, this)
                .then(function(res){
                    return File.build(res.data);
                });
        };

        File.prototype.setActivation = function(state) {
            return $http.post(
                fileConfig.statusesUrl.replace('%s', this.id),
                {state: state ? 'active' : 'inactive'})
                .then(function(res){
                    return File.build(res.data);
                });
        };

        File.get = function(id) {
            return $http.get(fileConfig.url + '/' + id)
                .then(function(res){
                    return File.build(res.data);
                });
        };

        File.build = function(data){
            return new File(data);
        };

        return File;
    });
