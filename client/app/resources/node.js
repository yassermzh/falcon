angular.module('resources.node', [])
    .constant("nodeConfig", {
        url: "/api/v1/nodes",
        nodeStatusesUrl: "/api/v1/nodes/%s/statuses",
        statusesUrl: "/api/v1/nodes/statuses"
    })
    .factory('Node', function($http, nodeConfig) {

        function Node(data) {
            _.merge(this, data);
        }

        Node.query = function() {
            return $http.get(nodeConfig.url)
                .then(function(res){
                    return _.map(res.data, function(d){ return Node.build(d); });
                });
        };

        // new node
        Node.prototype.add = function() {
            return $http.post(nodeConfig.url + '/', this)
                .then(function(res){
                    return Node.build(res.data);
                });
        };

        // edit node
        Node.prototype.edit = function() {
            return $http.put(nodeConfig.url + '/' + this.id, this)
                .then(function(res){
                    return Node.build(res.data);
                });
        };

        Node.prototype.setActivation = function(active) {
            return $http.post(
                nodeConfig.nodeStatusesUrl.replace('%s', this.id),
                {state: active ? 'active' : 'inactive'})
                .then(function(res){
                    return Node.build(res.data);
                });
        };

        Node.setStatuses = function(statuses) {
            return $http.post(
                nodeConfig.statusesUrl,
                {default: statuses.default})
                .then(function(res){
                    return Node.build(res.data);
                });
        };


        Node.get = function(id) {
            return $http.get(nodeConfig.url + '/' + id)
                .then(function(res){
                    return Node.build(res.data);
                });
        };

        Node.build = function(data){
            return new Node(data);
        };

        return Node;
    });
