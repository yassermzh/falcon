angular.module('av.toggleButton', [])
    .directive('avToggleButton', function(){
        return {

            restrict: 'E',
            scope: {
                title: '@',
                checked: '=',
                action: '&'
            },
            templateUrl: 'components/toggle-button/toggle-button.html',
            controller: 'AvToggleButtonCtrl'

        };
    })
    .controller('AvToggleButtonCtrl', function($scope){
        console.log('directive> checked=', $scope.checked);
        $scope.$watch('checked', function(newVal, oldValue){
            if (newVal==oldValue || oldValue==undefined) return;
            console.log('togglebutton> checked=', newVal);
            $scope.action();
        });
    });
