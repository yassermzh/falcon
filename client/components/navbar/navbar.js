'use strict';

angular.module('av.navbar', [])
    .directive('avNavbar', function(){
        return {
            restrict: 'E',
            controller: 'NavbarCtrl',
            replace: true,
            templateUrl: 'components/navbar/navbar.html'
        };
    })
    .controller('NavbarCtrl', function ($scope, $location) {

        console.log('navbarCtrl called');

        $scope.menu = [{
            'title': 'Nodes',
            'link': $scope.sitePrefix + '/nodes'
        }, {
            'title': 'Files',
            'link': $scope.sitePrefix + '/files'
        }];

        $scope.isCollapsed = true;

        $scope.isActive = function(route) {
            return route === $location.path();
        };
    });
