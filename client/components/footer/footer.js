angular.module('av.footer', [])

    .directive('avFooter', function() {
        return {
            restrict: 'E',
            controller: 'FooterCtrl',
            templateUrl: 'components/footer/footer.js'
        };
    })

    .controller('FooterCtrl', function() {
        console.log('footerCtrl called');
    });
