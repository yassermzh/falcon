angular.module('av.sortGrid', [])

    .directive('avSortGrid', function(){
        return {
            restrict: 'A',
            transclude: true,
            replace: true,
            scope: {
                data: '=',
                order: '='
            },
            template: '<div ng-transclude></div>',
            controller: 'SortGridCtrl'
        };
    })

    .controller('SortGridCtrl', function($scope){

        console.log('sortGridCtrl called');

        $scope.columns = [];

        this.registerColumn = function(column) {
            $scope.columns.push(column);
        };

        this.sortGrid = function (column){

            console.log('sortGrid called, column=', column);

            $scope.order.field = column.name;
            $scope.order.reverse = column.state=='down';

            _.each($scope.columns, function(_column) {
                if (_column.name == column.name) return;
                _column.state = null;
            });

        };

    })

    .directive('avSortColumn', function(){
        return {
            restrict: 'A',
            require: '^avSortGrid',
            transclude: true,
            scope: {
                default: '@'
            },
            templateUrl: 'components/sort-grid/sort-grid.html',
            link: function(scope, elem, attrs, sortGridCtrl) {
                console.log('sortByctrl called');
                var name = attrs['avSortColumn'];
                var column = scope.column = {
                    state: scope.default=='true' ? 'down' : null,
                    name: name
                };
                sortGridCtrl.registerColumn(scope.column);
                scope.changeState = function(){
                    switch(column.state) {
                    case 'up':
                        column.state = 'down';
                        break;
                    case 'down':
                        column.state = 'up';
                        break;
                    default:
                        column.state = 'down';
                        break;
                    }
                    console.log('column=', column);
                    sortGridCtrl.sortGrid(column);
                };

            }
        };
    });
