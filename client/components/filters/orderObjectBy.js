'use strict';

angular.module('av.filters', [])
    .filter('orderObjectBy', function() {
        return function(items, field, reverse) {
            console.log('orderObjectBy called, field=%s', field);
            var filtered = [];
            angular.forEach(items, function(item) {
                filtered.push(item);
            });
            filtered.sort(function (a, b) {
                return (a[field] > b[field] ? 1 : -1);
            });

            if(reverse) filtered.reverse();
            return filtered;
        };
    });
