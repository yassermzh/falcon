'use strict';

angular.module('av.sidebar', [])
    .directive('avSidebar', function(){
        return {
            restrict: 'E',
            controller: 'SidebarCtrl',
            replace: true,
            templateUrl: 'components/sidebar/sidebar.html'
        };
    })
    .controller('SidebarCtrl', function($scope) {

        var items = [
            {
                link: '#/nodes',
                title: 'ندها',
                children: [
                    {
                        link: '#/nodes',
                        title: 'فهرست'
                    }
                ]
            },
            {
                link: '#/files',
                title: 'فایل‌ها',
                children: [
                    {
                        link: '',
                        title: 'فهرست'
                    }
                ]

            }
        ];

        $scope.items = items;

    });
