angular.module('av.contentHeader', [])

    .directive('avContentHeader', function() {
        return {
            restrict: 'E',
            scope: {
                title: '@'
            },
            replace: true,
            controller: 'ContentHeaderCtrl',
            templateUrl: 'components/content-header/content-header.html'
        };
    })

    .controller('ContentHeaderCtrl', function() {
        console.log('ContentHeaderCtrl called');
    });
