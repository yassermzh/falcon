/*global require module */
'use strict';

var path = require('path');
var fs = require('fs');
var _ = require('lodash');


var multer  = require('multer');
var config = require('../../config/environment');
var tmpPath = path.join(config.root, '.tmp/uploads');
var upload = multer({ dest: tmpPath });
var uploadPath = path.join(config.root, '/server/uploads/');

var router = require('express').Router();

// type File struct {
//     Id           uint        `json:"id"`
//     Name         string      `json:"name"`
//     Size         uint64      `json:"size"`
//     Checksum     []byte      `json:"checksum"`
//     MimeType     string      `json:"mimeType"`
//     IsActive     bool        `json:"isActive"`
//     CreationTime time.Time   `json:"creationTime"`
// }


let files = [
    {
        name: "file1",
        path: "http://static.videogular.com/assets/videos/videogular.mp4",
        size: 111,
        mimeType: 'mp4',
        isActive: true,
        creationTime: '2014-10-10',
        id: 1

    },
    {
        name: "file22",
        path: "http://static.videogular.com/assets/videos/videogular.mp4",
        size: 222,
        mimeType: 'mp4',
        isActive: true,
        creationTime: '2013-12-10',
        id: 22
    },{
        name: "file3",
        path: "http://static.videogular.com/assets/videos/videogular.mp4",
        size: 333,
        mimeType: 'mp4',
        isActive: true,
        creationTime: '2014-10-12',
        id: 3
    },
];

router.get('/', (req, res) => {
    res.json(files);
});


router.get('/:id', (req, res) => {
    let id = req.params.id;
    console.log('id=%s!', id);
    let file = _.first(_.where(files, {id: +id}));
    console.log('file=', file);
    res.json(file);
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let data = req.body;
    let file = _.first(_.where(files, {id: +id}));
    console.log('data ->', data);
    _.merge(file, data);
    console.log('edit file ->', file);
    res.json(file);
});


router.post('/:id/statuses', (req, res) => {
    let state = req.body.state;
    let id = req.params.id;
    let file = _.first(_.where(files, {id: +id}));
    file.isActive = state == 'active';
    console.log('file=', file);
    res.json(file);
});


router.post('/',  upload.single('file'), (req, res) => {
    console.log('file=', req.file);
    console.log('tmpPath=%s, uploadPath=', tmpPath, uploadPath);
    var id = Math.max.apply(null, _.pluck(files, 'id')) + 1;
    fs.readFile(req.file.path, (err, data) => {
        var filename = '' + Date.now() + '.mp4';
        if (err) {
            console.error('failed to save');
            res.json({name: 'failed', id:1111});
            return;
        }
        var newPath = path.join(uploadPath, filename);
        console.log('file> save as filename=%s at %s', filename, newPath);
        fs.writeFile(newPath, data, function (err) {
            if (err) {
                console.error('failed to write');
                res.json({name: 'failed', id:1111});
                return;
            }
            var file = {
                name: '',
                id: id,
                creationTime: '2015-10-10',
                size: Math.ceil(Math.random()*100)
            };
            file.path = '/uploads/' + filename;
            files.push(file);
            console.log('file=', file);
            res.json(file);
        });
    });
});


module.exports =  router;
