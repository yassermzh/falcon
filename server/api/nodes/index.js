/*global require module */
'use strict';

var _ = require('lodash');
var router = require('express').Router();

// type Node struct {
//     Id           uint           `json:"id,omitempty"`
//     Host         string         `json:"host"`
//     Label        string         `json:"label"`
//     Quality      uint           `json:"capacity"`
//     Capacity     uint           `json:"quality"`
//     Location     string         `json:"location"`
//     Bandwidth    uint           `json:"bandwidth"`
//     StoragePath  string         `json:"storage_path"`
//     IsActive     bool           `json:"isActive"`
//     IsDefault    bool           `json:"isDefault"`
//     CreationTime time.Time      `json:"creationTime,omitempty"`
// }

let nodes = [
    {
        name: "node1",
        host: "http://falcon1.com",
        bandwidth: 121,
        isActive: true,
        creationTime: '2015-10-11',
        id: 1

    },
    {
        name: "node22",
        host: "http://falcon2.com",
        bandwidth: 1121,
        isActive: false,
        creationTime: '2015-10-15',
        id: 2
    },{
        name: "node3",
        host: "http://falcon3.com",
        bandwidth: 333,
        isActive: true,
        isDefault: true,
        creationTime: '2015-10-13',
        id: 3
    },
];

router.get('/', (req, res) => {
    res.json(nodes);
});


router.get('/:id', (req, res) => {
    let id = req.params.id;
    let node = _.first(_.where(nodes, {id: +id}));
    res.json(node);
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let data = req.body;
    let node = _.first(_.where(nodes, {id: +id}));
    console.log('data ->', data);
    _.merge(node, data);
    console.log('edit node ->', node);
    res.json(node);
});

router.post('/', (req, res) => {
    let node = req.body;
    var id = Math.max.apply(null, _.pluck(nodes, 'id')) + 1;
    node.id = id;
    console.log('node=', node);
    nodes.push(node);
    res.json(node);
});

router.post('/:id/statuses', (req, res) => {
    let state = req.body.state;
    let id = req.params.id;
    let node = _.first(_.where(nodes, {id: +id}));
    node.isActive = state == 'active';
    console.log('node=', node);
    res.json(node);
});


router.post('/statuses', (req, res) => {
    let id = req.body.default;
    let node = _.first(_.where(nodes, {id: +id}));
    _.each(nodes, (node) => {node.isDefault = false; } );
    node.isDefault = true;
    res.json(node);
});

module.exports =  router;
